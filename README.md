<div align="center">
    <h1 align="center">Thot</h1>
    <img src="./public/img/logo.png" alt="Thot logo" title="Logo de la app" width="256" height="256">
</div>
Thot, es una aplicación web de trivias muy sencilla de utilizar, te permite crear y jugar trivias, además de poder competir con otros jugadores por un puesto en ranking global. Tiene muchas aplicaciones, desde un mero entretenimiento entre amigos, hasta su uso como herramienta didáctica para el aula escolar. Es muy útil para reforzar conocimientos.

## Demostración del proyecto desplegado en un servidor 🚀
---
En el enlace de abajo se encuentra disponible un enlace del proyecto desplegado en un servidor de Heroku, a fin de demostración.

<a href="https://thot-web.herokuapp.com/" target="_blank">https://thot-web.herokuapp.com/</a>

## Instalación 🧱
---
Comandos para instalar y ejecutar la aplicación de forma local.
```
$ git clone git@gitlab.com:jahb24/thot.git
$ cd ./thot
$ npm install
$ npm start
```
## Demoastación la página principal del proyecto 👀 (Una imagen vale más que mil palabras)
---
<div align="center">
    <img src="https://firebasestorage.googleapis.com/v0/b/thot-14909.appspot.com/o/demo.png?alt=media&token=892f439e-9202-428a-8baa-d9dd7a284962" alt="Imagen de demostración" title="Demo">
</div>

## Plugins y software utilizado 📚
---
Aquí aparecen los plugins y software utilizados en el proyecto.
La documentación de los mismos está en los enlaces de abajo.

| Plugin y software | README |
| ------ | ------ |
| Firebase | https://firebase.google.com/docs/database |
| Socket IO | https://socket.io/docs/v4/ |
| Express | https://expressjs.com/es/ |
| Express-session | https://www.npmjs.com/package/express-session |
| connect-flash | https://www.npmjs.com/package/connect-flash |
| Node JS | https://nodejs.org/es/docs/ |
## Equipo de desarrollo ( Mapaches anónimos ) 🛠️
---
* ALEJANDRO ISAAC BACA SIERRA ( a320769@uach.mx )
* ARMANDO CHAVEZ PEREZ ( a316099@uach.mx )
* DANIEL LOPEZ VILLALOBOS ( a315120@uach.mx )
* EDWIN ALFREDO PINEDO CHACON ( a329549@uach.mx )
* JORGE AARON HERNANDEZ BUSTILLOS ( a329747@uach.mx )
## Agradecimientos ✨
---
- En primer lugar queremos agradecer a la maestra Olanda Prieto Ortiz, quien nos brindó de muchos conocimientos importantes del desarrollo de software y que aplicamos a través de cada una de las etapas de este proyecto para alcanzar los resultados que buscábamos en este proyecto.
- También queremos agradecer a Stack Overflow, a todas esas personas de la comunidad que tuvieron las mismas dudas y problemas que nosotros, y a quienes las contestaron. Así como todos esos tutoriales y páginas de internet de las que sacamos el conocimiento necesario para el desarrollo de nuestra aplicación.
- Al equipo desarrollo ( Mapaches anónimos ) quienes siempre tuvieron la actitud y disponibilidad necesaria para enfrentar los retos que se presentaron.
## Licencia ✔️
---
#### MIT
##### Software gratis, matanga... 🏃 
