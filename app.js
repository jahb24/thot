const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');
const SocketIO = require('socket.io');
const flash = require('connect-flash');

// Express
const app = express();
const port = process.env.PORT || 3000;

// Body-parse
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Session
app.use(session({
    secret: process.env.SESSION_SECRET || 'U8^D^X2$!^DC$no9J^hA7THOT!ZhTDIhOixRF5znQ9U2lH6d0j',
    cookie: { maxAge: 14400000 }, // 4 hours (session time).
    resave: true,
    saveUninitialized: false
}));

// Flash
app.use(flash());

// Template engine
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');

// Public static
app.use(express.static(__dirname + "/public"));

// Routers
const indexRouter = require('./routes/index');
const registerRouter = require('./routes/register');
const loginRouter = require('./routes/login');
const contactRouter = require('./routes/contact');
const roomRouter = require('./routes/room');
const profileRouter = require('./routes/profile');
const rankingRouter = require('./routes/ranking');
const makerRouter = require('./routes/maker');
const mytriviasRouter = require('./routes/mytrivias');
const triviaRouter = require('./routes/trivia');

// Middlewares
app.use('/', indexRouter);
app.use('/register', registerRouter);
app.use('/login', loginRouter);
app.use('/contact', contactRouter);
app.use('/room', roomRouter);
app.use('/profile', profileRouter);
app.use('/ranking', rankingRouter);
app.use('/maker', makerRouter);
app.use('/mytrivias', mytriviasRouter);
app.use('/trivia', triviaRouter);

// Page 404
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + "/public/404.html");
});

// Lisener
const server = app.listen(port, () => {
    console.log('Servidor a su servicio en el puerto: ', port);
});

// Web Sockets
const io = SocketIO(server);
global.io = io;

const firebase = require('./config/firebase');

io.on('connection', (socket) => {

    // create a room to play (Host)
    socket.on('hostRoom', async(data) => {

        let trivia;

        // get the trivia
        await firebase.db.collection("trivias").doc(data.TRIVIA_ID).get().then((doc) => trivia = doc.data());

        // create the room with firebase (insert the trivia)
        await firebase.firebase.database().ref('games/' + data.ROOM_ID).set({ trivia: trivia });
        //await firebase.firebase.database().ref('games/' + data.ROOM_ID + '/users/' + socket.id).set({ nickname: data.user, score: 0 });

        socket.join(data.ROOM_ID);

        let playersList;

        const dbRef = firebase.firebase.database().ref();
        await dbRef.child("games").child(data.ROOM_ID).child("users").get().then((snapshot) => {
            if (snapshot.exists()) {
                playersList = snapshot.val();
            } else {
                playersList = { info: "No hay jugadores disponibles" }
            }
        });

        socket.emit('trivia', { trivia: trivia });
        io.to(data.ROOM_ID).emit('updateRoom', { playersList: playersList }); //Sending host player data to display
    });

    // join a room (client)
    socket.on('joinRoom', async(data) => {

        const dbRef = firebase.firebase.database().ref();

        await dbRef.child("games").child(data.ROOM_ID).get().then(async(snapshot) => { // Chech if exist the room
            if (snapshot.exists()) {

                let playersList;

                // get the host's player list
                await dbRef.child("games").child(data.ROOM_ID).child("users").get().then((snapshot) => {
                    if (snapshot.exists()) {
                        playersList = snapshot.val();
                    } else {
                        playersList = { info: "No hay jugadores disponibles" }
                    }
                });

                Object.size = function(obj) {
                    var size = 0,
                        key;
                    for (key in obj) {
                        if (obj.hasOwnProperty(key)) size++;
                    }
                    return size;
                };

                // chech number of players
                if (Object.size(playersList) > 30) {
                    socket.emit('canJoin', { join: false, message: "La sala de juego a la que intenta acceder, está a su máxima capacidad (30 jugadores)." });
                    return;
                }

                // add the user to the room's user list
                await firebase.firebase.database().ref('games/' + data.ROOM_ID + '/users/' + socket.id).set({ nickname: data.user, score: 0, answer: "" });

                // join user
                socket.join(data.ROOM_ID);

                playersList[socket.id] = {
                    nickname: data.user,
                    score: 0,
                    answer: ""
                };

                io.to(data.ROOM_ID).emit('updateRoom', { playersList: playersList }); //Sending host player data to display
                socket.emit('canJoin', { join: true, userID: socket.id });
            } else {
                // Room don't exist
                socket.emit('canJoin', { join: false, message: "La sala de juego que ha ingresada, no existe." });
            }
        });
    });

    // save client answer
    socket.on('client:answer', async(data) => {

        await firebase.firebase.database().ref('games/' + data.ROOM_ID + '/users/' + socket.id).update({ answer: data.answer });
        io.to(data.ROOM_ID).emit('finish', {}); // send ranking
    });

    // get ranking
    socket.on('getRanking', async(data) => {

        let heightest = [];

        // get all users
        const dbRef = firebase.firebase.database().ref();
        await dbRef.child("games").child(data.ROOM_ID).child("users").get().then((snapshot) => {
            let users = snapshot.val();

            for (const player in users) {
                heightest.push({
                    nickname: users[player].nickname,
                    score: users[player].score
                });
            }

            io.to(data.ROOM_ID).emit('ranking', { ranking: heightest }); // send ranking to host

            // delete old answers
            for (const player in users) firebase.firebase.database().ref('games/' + data.ROOM_ID + '/users/' + player).update({ answer: "" });
        });
    });

    // check answers
    socket.on('checkAnswers', async(data) => {
        io.to(data.ROOM_ID).emit('isCorrect', { answer: data.answer }); // send answer to clients
    });

    // update player score
    socket.on('updateScore', async(data) => {
        await firebase.firebase.database().ref('games/' + data.ROOM_ID + '/users/' + socket.id).update({ score: data.score });
    });

    // send question to clients
    socket.on('sendQuestion', (data) => {
        io.to(data.ROOM_ID).emit('question', { quest: data.quest });
    });

    // time update (client)
    socket.on('roomInfo', (data) => {
        io.to(data.ROOM_ID).emit('infoClient', { time: data.time, no_questions: data.no_questions, currentQuestion: data.currentQuestion }); //Sending cleint player time to display
    });

    // end game
    socket.on('finish', async(data) => {

        io.to(data.ROOM_ID).emit('gameOver', {}); // game over (for clients)

        // leave the room
        socket.leave(data.ROOM_ID);

        // delete the trivia
        const dbRef = firebase.firebase.database().ref();
        await dbRef.child("games").child(data.ROOM_ID).set(null);
        await dbRef.child("games").child(data.ROOM_ID).remove();
    });

    // save experience
    socket.on('saveExp', (data) => {

        // leave the room
        socket.leave(data.ROOM_ID);
    });
});