const express = require('express');
//Firebase
const firebase = require('firebase');

const app = firebase.initializeApp({
    apiKey: "AIzaSyAeDSKC3e81JANT4F5V3VxOJCSJRAElQlM",
    authDomain: "thot-14909.firebaseapp.com",
    projectId: "thot-14909",
    storageBucket: "thot-14909.appspot.com",
    messagingSenderId: "444888401034",
    appId: "1:444888401034:web:2deb24173a1b3319269973"
});

const db = firebase.firestore();

module.exports = { db, app, firebase }