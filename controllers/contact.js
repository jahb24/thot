const firebase = require('../config/firebase');

// Post method
const sendMessage = async(req, res, next) => {

    // From information
    const nickname = req.body.nickname;
    const email = req.body.email;
    const message = req.body.message;

    // firebase insertion
    await firebase.db.collection("contact").add({
            nickname: nickname,
            email: email,
            message: message
        })
        .then((docRef) => { // Sucessful
            let info = { messageType: 0, message: "El mensaje se ha enviado correctamente ;)" };
            req.flash('info', info);
            res.redirect('/');
        })
        .catch((error) => { // Error
            let errorMessage = error.message;;

            let info = { messageType: 1, message: errorMessage }
            req.flash('info', info);

            res.status(401).redirect('/');
        });
}

module.exports = {
    sendMessage
}