const firebase = require('../config/firebase');

// Function to sign out
const signOutUser = async(req, res, next) => {
    await firebase.app.auth().signOut().then(async() => {
        // Sign-out successful.
        req.session.user = undefined;
        res.redirect('/');
    }).catch((error) => {
        // Logout error
        req.flash('info', error);
        console.log(error);
        res.status(400).redirect('/');
    });
}

// Function to get the trivias
const getTrivias = async(categorie) => {

    // Categorie parameter
    let param = categorie ? categorie : 0;
    param = parseInt(param);

    // Query by category
    let query = firebase.db.collection('trivias');
    if (param != 0) query = query.where("info.categorie", "==", param); // It will fetch only one category

    // list for the trivias
    let info = [];

    // Get the trivias
    const data = await query.get();
    trivias = data.docs.map(doc => {

        let trivia = doc.data().info

        // Save trivia ID
        trivia.id = doc.id;

        // Save trivia dificult like a String
        if (trivia.dificult == 1) trivia.dificult = "Muy fácil";
        else if (trivia.dificult == 2) trivia.dificult = "Fácil";
        else if (trivia.dificult == 3) trivia.dificult = "Media";
        else if (trivia.dificult == 4) trivia.dificult = "Difícil";

        // Save trivia categorie like a String
        if (trivia.categorie == 1) trivia.categorie = "Bíologia";
        else if (trivia.categorie == 2) trivia.categorie = "Ecología";
        else if (trivia.categorie == 3) trivia.categorie = "Geografía";
        else if (trivia.categorie == 4) trivia.categorie = "Historia";
        else if (trivia.categorie == 5) trivia.categorie = "Inglés";
        else if (trivia.categorie == 6) trivia.categorie = "Literatura";
        else if (trivia.categorie == 7) trivia.categorie = "Química";
        else if (trivia.categorie == 8) trivia.categorie = "Matemáticas";
        else if (trivia.categorie == 9) trivia.categorie = "Tecnología";
        else trivia.categorie = "Otros";

        // Add the trivia to the list
        info.push(trivia);
    });

    return info;
}

// Save experience
const saveExp = async(req, res) => {

    let user = req.session.user;

    let exp = req.exp / 20000;
    console.log("Exp: " + exp);
    if (user) await firebase.db.collection("users").doc(req.session.user.uid).update({ level: firebase.firebase.firestore.FieldValue.increment(exp) });

    res.redirect('/');
}

// Exports
module.exports = {
    signOutUser,
    getTrivias,
    saveExp
}