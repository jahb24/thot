const firebase = require('../config/firebase');

const loginUser = async(req, res, next) => {

    const nickname = req.body.nickname;
    const password = req.body.password;

    // Authentication
    await firebase.app.auth().signInWithEmailAndPassword(nickname + "@abcde.com", password)
        .then(async(userCredential) => {
            // Signed in
            //console.log("Exito");
            let user = userCredential.user;

            // Read user's firestore info
            await firebase.db.collection("users").doc(user.uid).get().then((doc) => {
                if (doc.exists) {
                    req.session.user = { nickname: nickname, uid: user.uid, level: doc.data().level, trivia: doc.data().trivia };
                } else {
                    throw "El usuario \"" + nickname + "\" no existe.";
                }
            }).catch((error) => {
                throw error;
            });

            res.redirect('/'); // redirect to index
        })
        .catch((error) => {
            //let errorCode = error.code;
            let errorMessage;

            switch (error.code) {
                case "auth/wrong-password":
                    errorMessage = "Tú contraseña es incorrecta.";
                    break;
                case "auth/user-not-found":
                    errorMessage = "El usuario con el nickname \"" + nickname + "\" no existe.";
                    break;
                case "ERROR_USER_DISABLED":
                    errorMessage = "El usuario con este nickname ha sido deshabilidtado.";
                    break;
                case "ERROR_TOO_MANY_REQUESTS":
                    errorMessage = "Demaciadas peticiones. Intentalo mpas tarde.";
                    break;
                default:
                    errorMessage = error.message;
            }

            //console.log(errorCode);
            //console.log(errorMessage);

            let info = { messageType: 1, message: errorMessage };
            req.flash('info', info);

            res.status(400).redirect('/login');
        });
}

module.exports = {
    loginUser
}