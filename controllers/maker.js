const firebase = require('../config/firebase');

// Post method. save a trivia
const saveTrivia = async(req, res, next) => {

    // Trivia information
    const trivia = req.body.trivia;

    const nickname = req.session.user.nickname;
    const categorie = trivia.categorie;
    const dificult = trivia.dificult;
    const no_questions = trivia.no_questions;
    const time = trivia.time;
    const name = trivia.name;

    const info = {
        author: nickname,
        categorie: categorie,
        dificult: dificult,
        no_questions: no_questions,
        time: time,
        name: name
    }

    delete trivia.categorie, trivia.dificult, trivia.no_questions, trivia.time, trivia.name;

    // firebase insertion
    await firebase.db.collection("trivias").add({
            info: info,
            quiz: trivia.question
        })
        .then(async(docRef) => { // Sucessful

            // Save the trivia in the user's trivia list
            await firebase.db.collection("users").doc(req.session.user.uid).update({
                    trivia: firebase.firebase.firestore.FieldValue.arrayUnion(docRef.id) // Add the new trivia ID
                })
                .then(() => { // Sucessful
                    req.session.user.trivia.push(docRef.id);
                    res.status(200).redirect('/');
                }).catch((error) => {
                    throw error;
                });
        })
        .catch((error) => { // Error
            let errorMessage = error.message;

            let info = { messageType: 1, message: errorMessage }
            req.flash('info', info);

            res.status(401).redirect('/');
        });
}

module.exports = {
    saveTrivia
}