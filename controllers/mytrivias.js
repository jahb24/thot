const firebase = require('../config/firebase');

// Function to get the trivias
const getTrivias = async(triviaList) => {

    // Query by category
    let query = firebase.db.collection('trivias').where(firebase.firebase.firestore.FieldPath.documentId(), "in", triviaList); // query

    // list for the trivias
    let info = [];

    // Get the trivias
    const data = await query.get();
    trivias = data.docs.map(doc => {

        let trivia = doc.data().info

        // Save trivia ID
        trivia.id = doc.id;

        // Save trivia dificult like a String
        if (trivia.dificult == 1) trivia.dificult = "Muy fácil";
        else if (trivia.dificult == 2) trivia.dificult = "Fácil";
        else if (trivia.dificult == 3) trivia.dificult = "Media";
        else if (trivia.dificult == 4) trivia.dificult = "Difícil";

        // Save trivia categorie like a String
        if (trivia.categorie == 1) trivia.categorie = "Bíologia";
        else if (trivia.categorie == 2) trivia.categorie = "Ecología";
        else if (trivia.categorie == 3) trivia.categorie = "Geografía";
        else if (trivia.categorie == 4) trivia.categorie = "Historia";
        else if (trivia.categorie == 5) trivia.categorie = "Inglés";
        else if (trivia.categorie == 6) trivia.categorie = "Literatura";
        else if (trivia.categorie == 7) trivia.categorie = "Química";
        else if (trivia.categorie == 8) trivia.categorie = "Matemáticas";
        else if (trivia.categorie == 9) trivia.categorie = "Tecnología";
        else trivia.categorie = "Otros";

        // Add the trivia to the list
        info.push(trivia);
    });

    return info;
}

// Function to get the users list
const deleteTrivia = async(req, res, next) => {

    // This is the ID of the trivia that will be deleted
    let triviaID = req.body.triviaID;

    // Delete a trivia
    await firebase.db.collection("trivias").doc(triviaID).delete().then(async() => {

        // Delete the trivia from user's trivia list
        await firebase.db.collection("users").doc(req.session.user.uid).update({
                trivia: firebase.firebase.firestore.FieldValue.arrayRemove(triviaID) // Delete the trivia with this ID
            })
            .then(() => { // Sucessful
                res.redirect('/mytrivias');
            }).catch((error) => { // Error
                throw error;
            });

    }).catch((error) => { // Catch the error
        console.error("Error removing document: ", error);
    });
}

module.exports = {
    deleteTrivia,
    getTrivias
};