const firebase = require('../config/firebase');

// Function to get the users list
const getList = async(req, res, next) => {

    let list = [];

    // Database request
    await firebase.db.collection("users").orderBy("level", "desc").get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            //console.log(`${doc.id} => ${doc.data()}`);
            let element = {};
            element.nickname = doc.data().nickname;
            element.level = doc.data().level;
            element.experience = doc.data().experience;
            list.push(element);
        });
    });
    return list;
}

module.exports = {
    getList
};