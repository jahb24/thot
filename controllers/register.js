const firebase = require('../config/firebase');

// Post method. Register a user
const registerUser = async(req, res, next) => {

    // From information
    const nickname = req.body.nickname;
    const password = req.body.password;

    await firebase.app.auth().createUserWithEmailAndPassword(nickname + "@abcde.com", password)
        .then(async(userCredential) => {
            // Signed in
            let user = userCredential.user;
            // firestore - user info
            await firebase.db.collection("users").doc(user.uid).set({
                    nickname: nickname,
                    level: 1.0,
                    trivia: []
                })
                .then((docRef) => {
                    //console.log("Document written with ID: ", docRef.id);
                    let info = { messageType: 0, message: nickname };
                    req.flash('info', info);
                    res.redirect('login');
                })
                .catch((error) => {
                    //console.log(error);
                    throw error;
                });
        })
        .catch((error) => {
            //let errorCode = error.code;
            let errorMessage;

            // Error message
            switch (error.code) {
                case "email-already-in-use":
                case "auth/email-already-in-use":
                    errorMessage = "El nickname ingresado ya existe.";
                    break;
                case "ERROR_TOO_MANY_REQUESTS":
                    errorMessage = "Demaciadas peticiones. Intentalo mpas tarde.";
                    break;
                default:
                    errorMessage = error.message;
            }

            let info = { messageType: 1, message: errorMessage }
            req.flash('info', info);

            res.status(401).redirect('/register');
        });
}

module.exports = {
    registerUser
}