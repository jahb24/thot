const firebase = require('../config/firebase');

// Save experience
const saveExp = async(req, res) => {

    let user = req.session.user;

    let exp = req.body.exp / 20000;

    if (user) {
        req.session.user.level += exp;
        await firebase.db.collection("users").doc(req.session.user.uid).update({ level: firebase.firebase.firestore.FieldValue.increment(exp) });
    }

    res.redirect('/');
}

// Exports
module.exports = {
    saveExp
}