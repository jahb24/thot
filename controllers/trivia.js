const firebase = require('../config/firebase');

// Function to get the trivia selected
const getTrivia = async(triviaID) => {

    // Trivia object
    let trivia;

    await firebase.db.collection("trivias").doc(triviaID).get().then((doc) => {
        if (doc.exists) { // Successful
            trivia = doc.data();
        } else {
            throw "La trivia con ID \"" + triviaID + "\" no existe.";
        }
    }).catch((error) => { // Error
        let info = { messageType: 1, message: errorMessage };
        req.flash('info', info);

        res.status(400).redirect('/login');
    });

    return trivia;
}

// Exports
module.exports = {
    getTrivia
}