/*
    << Trivia maker code >>

    Categorie

    > 1  : Biología
    > 2  : Ecología
    > 3  : Geografía
    > 4  : Historia
    > 5  : Inglés
    > 6  : Literatura
    > 7  : Química
    > 8  : Matemáticas
    > 9  : Tecnología
    > 10 : Otros

    Dificult

    > 1 : Muy fácil
    > 2 : Fácil
    > 3 : Medio
    > 4 : Difícil

    trivia = {
        categorie: Integer
        dificult: Integer
        time: Integer
        no_questions: Integer
        question1: []
    }
*/

// main div
var canvas = document.getElementById("canvas");

// trivia object
var trivia;
// current question
var current_question = 0;

// function to start to create a trivia
const start = () => {

    let categorie;
    let dificult;
    let time;
    let no_questions;

    let select;

    select = document.getElementById("categorie");
    categorie = parseInt(select.options[select.selectedIndex].value);

    for (let i = 1; i < 5; i++) {
        if (document.getElementById(`gridRadios${i}`).checked) {
            dificult = parseInt(document.getElementById(`gridRadios${i}`).value);
        }
    }

    select = document.getElementById("time");
    time = parseInt(select.options[select.selectedIndex].value);

    select = document.getElementById("questions");
    no_questions = parseInt(select.options[select.selectedIndex].value);

    // trivia object
    trivia = {
        categorie: categorie,
        dificult: dificult,
        time: time,
        no_questions: no_questions,
        question: []
    };

    drawCanvasQuestion();
};

const next = () => {
    if (current_question < trivia.no_questions - 1 && validateForm()) {
        saveQuestion();
        current_question++;
        drawCanvasQuestion();
    }
};

const back = () => {
    if (current_question > 0) {
        current_question--;
        drawCanvasQuestion();
    }
};

// function to draw the question canvas
const drawCanvasQuestion = () => {
    canvas.innerHTML = `
    <div class="card-question" id="canvas">
        <h1 style="font-weight: bold; text-align: center;">Pregunta ${current_question+1}/${trivia.no_questions}</h1>
        <input class="form-control" name="question" id="question" placeholder="Pregunta" maxlength="70" style="font-weight: bold;" required>
        <input class="form-control" id="answer" placeholder="Respuesta correcta" maxlength="38" style="color:green;" required>
        <input class="form-control" id="wrong1" placeholder="Respuesta incorrecta" maxlength="38" style="color:red;" required>
        <input class="form-control" id="wrong2" placeholder="Respuesta incorrecta" maxlength="38" style="color:red;" required>
        <input class="form-control" id="wrong3" placeholder="Respuesta incorrecta" maxlength="38" style="color:red;" required>
        <button class="btn btn-primary btn-lg" id="bac" style="width:49%" onclick="back()" title="Volver">Volver</button>
        <button class="btn btn-primary btn-lg" id="nex" style="background-color:#28a745; width:49%" onclick="next()" title="Continuar">Continuar</button>
        <button class="btn btn-primary btn-lg" id="nex" style="background-color:#d9534f; width:100%" onclick="cancel()" title="Cancelar todo">Cancelar todo</button>
    </div>
    `;

    if (current_question == trivia.no_questions - 1) {
        let nextButton = document.getElementById("nex");
        nextButton.firstChild.data = "Finalizar";
        nextButton.onclick = giveName;
    }
    if (current_question == 0) document.getElementById("bac").disabled = true;

    if (trivia.question[current_question] != undefined) {
        document.getElementById("question").value = trivia.question[current_question].question;
        document.getElementById("answer").value = trivia.question[current_question].answer;
        document.getElementById("wrong1").value = trivia.question[current_question].wrong1;
        document.getElementById("wrong2").value = trivia.question[current_question].wrong2;
        document.getElementById("wrong3").value = trivia.question[current_question].wrong3;
    }
}

// Function for form validation
function validateForm() {
    let a = document.getElementById("question").value;
    let b = document.getElementById("answer").value;
    let c = document.getElementById("wrong1").value;
    let d = document.getElementById("wrong2").value;
    let e = document.getElementById("wrong3").value;
    if (a == "" || b == "" || c == "" || d == "" || e == "") {
        alert("Por favor, llena todos los campos antes de continuar.");
        return false;
    } else {
        return true;
    }
}

// function to send the trivia
const sendTrivia = () => {
    document.getElementById("terminus").disabled = true; // block button

    // Sending and receiving data in JSON format using POST method
    let xhr = new XMLHttpRequest();
    xhr.open("POST", "/maker", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function() { //Call a function when the state changes.
        if (xhr.readyState == 4 && xhr.status == 200) {
            alert('Tu trivia se ha creado correctamente');
            window.location = '/';
        }
    }
    xhr.send(JSON.stringify({
        trivia: trivia
    }));
}

// function to give a name to the trivia
const giveName = () => {
    if (validateForm()) {
        saveQuestion();

        canvas.innerHTML = `
        <div class="card-question" id="canvas">
            <h1 style="font-weight: bold; text-align: center;">Nombre de la trivia</h1>
            <input class="form-control" name="trivianame" id="trivianame" placeholder="Nombre de la trivia" maxlength="32" style="font-weight: bold;" required>
            <button class="btn btn-primary btn-lg" id="terminus" style="background-color:#28a745; width:100%" onclick="sendVer()" title="Finalizar trivia">Finalizar trivia</button>
            <button class="btn btn-primary btn-lg" id="can" style="background-color:#d9534f; width:100%" onclick="cancel()" title="Cancelar todo">Cancelar todo</button>
        </div>
        `;
    }
}

// function to verify the name's form
const sendVer = () => {
    let name = document.getElementById("trivianame").value;
    if (name != "") {
        trivia.name = name;
        sendTrivia();
    } else {
        alert('Necesitas ingresar un nombre para la trivia.');
    }
}

// function to cancel all
const cancel = () => {
    window.location = '/';
}

// function to save the current question
const saveQuestion = () => {

    let question = document.getElementById("question").value;
    let answer = document.getElementById("answer").value;
    let wrong1 = document.getElementById("wrong1").value;
    let wrong2 = document.getElementById("wrong2").value;
    let wrong3 = document.getElementById("wrong3").value;

    trivia.question[current_question] = {
        question: question,
        answer: answer,
        wrong1: wrong1,
        wrong2: wrong2,
        wrong3: wrong3
    }

    console.log(trivia.question[current_question]);
}