// Socket ( client-server comunication )
const socket = io();

// main div
var canvas = document.getElementById("canvas");

// user name
var user;
var userID;
var myScore = 0;
var myAnswer = "";

var globalTime = 0;
var globalTimeMax = 20;

// trivia ID
var triviaID;

// option buttons
var optionA;
var optionB;
var optionC;
var optionD;

// function to join a trivia
const joinRoom = () => {

    user = document.getElementById("nickname").value;
    triviaID = document.getElementById("code").value;

    if (triviaID.length == 0) {
        alert('Porfavor, ingresé el código del juego.');
        return;
    }
    if (user.length == 0) {
        alert('Porfavor, ingresé un nickname.');
        return;
    }

    // try to connect with triviaID
    socket.emit('joinRoom', { ROOM_ID: triviaID, user: user });
}

// try to connect
socket.on('canJoin', (data) => {
    if (data.join) {
        userID = data.userID;
        roomDraw();
    } else alert(data.message);
});

// time update
socket.on('infoClient', (data) => {
    let time = document.getElementById("timePax");
    let num_quest = document.getElementById("no_question");
    if (time && num_quest) {
        num_quest.innerHTML = `Pregunta: ${data.currentQuestion+1}/${data.no_questions}`;
        time.innerHTML = `Tiempo: ${data.time}`;
        globalTime = data.time;
        if (globalTime > 24) globalTimeMax = 30;
    }
});

// Check if my answer is correct
socket.on('isCorrect', (data) => {
    if (myAnswer == data.answer) correctDraw();
    else incorrectDraw();
    myAnswer = "";
});

// question update
socket.on('question', (data) => {
    questionDraw();
    document.getElementById("question").innerHTML = data.quest.question;

    document.getElementById("op1").innerHTML = data.quest.options[0];
    document.getElementById("op1").value = data.quest.options[0];

    document.getElementById("op2").innerHTML = data.quest.options[1];
    document.getElementById("op2").value = data.quest.options[1];

    document.getElementById("op3").innerHTML = data.quest.options[2];
    document.getElementById("op3").value = data.quest.options[2];

    document.getElementById("op4").innerHTML = data.quest.options[3];
    document.getElementById("op4").value = data.quest.options[3];
});

// waiting for the game over
socket.on('gameOver', () => {
    socket.emit('saveExp', { ROOM_ID: triviaID, exp: myScore }); // Save my experience, please.
    gameOverDraw();
});

// function to draw a waiting screen
const roomDraw = () => {
    canvas.innerHTML = `
    <h2 style="color:white;text-align:center; height:30px;">Esperando a que el host inicie el juego...</h2>
    <img src="https://i.gifer.com/origin/f5/f5baef4b6b6677020ab8d091ef78a3bc_w200.gif" style="position:absolute;left:45%;height:200px;width:200px;" alt="Esperando...">
    `;
}

// function to wait the next question
const waitDraw = () => {
    canvas.innerHTML = `
    <h2 style="color:white;text-align:center; height:30px;">Esperando a que los otros jugadores terminen...</h2>
    <img src="https://i.gifer.com/origin/f5/f5baef4b6b6677020ab8d091ef78a3bc_w200.gif" style="position:absolute;left:45%;height:200px;width:200px;" alt="Esperando...">
    `;
}

// function to draw the questions
const questionDraw = () => {
    canvas.innerHTML = `
    <div style="position: absolute;left:0%;top: 0%;font-family: cursive;font-weight: bold;font-size: 150%;text-align: center;" id="timePax">Tiempo:</div>

    <div class="container">
        <div class="question-area" id="question">Question</div>
        <div class="answer1" id="btn1"><button id="op1">A</button></div>
        <div class="answer2" id="btn2"><button id="op2">B</button></div>
        <div class="answer3" id="btn3"><button id="op3">C</button></div>
        <div class="answer4" id="btn4"><button id="op4">D</button></div>
        <div class="no-question" id="no_question">Pregunta #/#</div>
        <div class="information" id="information">Tus puntos: ${myScore}</div>
    </div>
    `;

    // Get every button DOM
    optionA = document.getElementById("op1");
    optionB = document.getElementById("op2");
    optionC = document.getElementById("op3");
    optionD = document.getElementById("op4");

    // Add a event lisener to every button
    optionA.addEventListener('click', e => {
        e.preventDefault();
        socket.emit('client:answer', {
            ROOM_ID: triviaID,
            answer: optionA.value
        });
        myAnswer = optionA.value;
        waitDraw();
    });

    optionB.addEventListener('click', e => {
        e.preventDefault();
        socket.emit('client:answer', {
            ROOM_ID: triviaID,
            answer: optionB.value
        });
        myAnswer = optionB.value;
        waitDraw();
    });

    optionC.addEventListener('click', e => {
        e.preventDefault();
        socket.emit('client:answer', {
            ROOM_ID: triviaID,
            answer: optionC.value
        });
        myAnswer = optionC.value;
        waitDraw();
    });

    optionD.addEventListener('click', e => {
        e.preventDefault();
        socket.emit('client:answer', {
            ROOM_ID: triviaID,
            answer: optionD.value
        });
        myAnswer = optionD.value;
        waitDraw();
    });
}

// function to draw a correct answer screen
const correctDraw = () => {
    let scorePlus = parseInt(1000 - (950 / globalTimeMax) * (globalTimeMax - globalTime));
    myScore += scorePlus;
    canvas.innerHTML = `
    <h2 style="color:white;text-align:center; height:30px;">¡ Correcto !</h2>
    <h2 style="color:white;text-align:center; height:30px;">+ ${scorePlus} puntos</h2>
    <h2 style="color:white;text-align:center; height:30px;"> Tu puntuación actual es: ${myScore} puntos</h2>
    <img src="https://thumbs.gfycat.com/ConstantBlaringHornedviper-max-1mb.gif" style="position:absolute;left:45%;top:30%;height:200px;width:200px;" alt="Esperando...">
    `;
    socket.emit('updateScore', { ROOM_ID: triviaID, score: myScore });
}

// function to draw a wrong answer screen
const incorrectDraw = () => {
    canvas.innerHTML = `
    <h2 style="color:white;text-align:center; height:30px;">Tu respuesta fue incorrecta</h2>
    <h2 style="color:white;text-align:center; height:30px;">+ 0 puntos</h2>
    <img src="https://vignette.wikia.nocookie.net/youtubepedia/images/4/43/HomeroGIF.gif/revision/latest?cb=20170512192057&path-prefix=es" style="position:absolute;left:45%;height:200px;width:200px;" alt="Esperando...">
    `;
}

// function to draw a game over screen
const gameOverDraw = () => {
    lastMessage = (myScore < 4500) ? "Muy bien, " + user + "." : "¡ Excelente, " + user + " !";
    canvas.innerHTML = `
    <h2 style="color:white;text-align:center; height:30px; font-size: 48px;">Game Over</h2>
    <br/>
    <h2 style="color:white;text-align:center; height:30px;">${lastMessage}</h2>
    <h2 style="color:white;text-align:center; height:30px;">Tu puntuación final: ${myScore}</h2>
    <h2 style="color:white;text-align:center; height:30px;">Experiencia ganada: ${parseInt(myScore/200)} puntos</h2>
    <form action="/room" method="POST">
        <input type="hidden" name="exp" id="exp" value=${myScore}>
        <input type="submit" class="start" style="background-color:#d9534f;color:white;font-size: 48px" value="Salir">
    </form>
    <img src="https://c.tenor.com/tCksS1cRuTAAAAAM/gato.gif" style="position:absolute;left:45%;top:35%;height:200px;width:200px;" alt="Esperando...">
    `;
}