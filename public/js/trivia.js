// Socket ( client-server comunication )
const socket = io();

// main div
var canvas = document.getElementById("canvas");

// trivia ID
const triviaID = document.getElementById("triviaID").value;

// number of players
var numPlayers = 0;

// number of answers;
var numAnswers = 0;

// Players in the lobby
var playersList = "";

// trivia
var trivia;
var currentQuestion = 0;
var currentTime;
var quiz = [];

// ranking local
var ranking = {};

// get the trivia
socket.on('trivia', (data) => {
    trivia = data.trivia;
    trivia.quiz.forEach(question => quiz.push(question));
});

// function to generate room codes
const generateRandomString = (num) => {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let result1 = '';
    const charactersLength = characters.length;
    for (let i = 0; i < num; i++) {
        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result1;
}

// ROOM CODE
const ROOM_CODE = generateRandomString(6);

// this function desorder an array
function desorder(array) {
    array = array.sort(function() { return Math.random() - 0.5 });
    return array;
}

// function to get the current question
const getQuestion = () => {

    quest = {}
    options = []

    quest.question = quiz[currentQuestion].question;
    options.push(quiz[currentQuestion].answer);
    options.push(quiz[currentQuestion].wrong1);
    options.push(quiz[currentQuestion].wrong2);
    options.push(quiz[currentQuestion].wrong3);
    options = desorder(options);

    quest.options = options;

    return quest;
}

// function to start the trivia
const startGame = async() => {
    if (numPlayers < 1) {
        alert('Falta 1 jugador');
        return;
    }
    if (trivia) {
        for (currentQuestion = 0; currentQuestion < trivia.info.no_questions; currentQuestion++) {

            currentTime = trivia.info.time;
            numAnswers = 0;
            quest = getQuestion();

            socket.emit('sendQuestion', { ROOM_ID: ROOM_CODE, quest: quest });
            await answeringDraw();
            socket.emit('checkAnswers', { ROOM_ID: ROOM_CODE, answer: quiz[currentQuestion].answer });
            await resultDraw();
            await rankingDraw();
        }
    }
    socket.emit('finish', { ROOM_ID: ROOM_CODE });
    document.getElementById("theEnd").innerHTML = `
    <button class="start" style="background-color:#d9534f;position:absolute;left:0%;bottom:10%;" onclick=" window.location = '/';">Salir</button>
    `;
}

// function to draw the waiting for answers
const answeringDraw = async() => {
    while (currentTime >= 0) {
        canvas.innerHTML = `
        <h1 style="font-weight:bold;">Pregunta: ${currentQuestion+1}/${trivia.info.no_questions}</h1>
        <h1 style="font-weight:bold;">Tiempo: ${currentTime}</h1>
        <h1 style="font-weight:bold; color:green;">Pregunta: ${quiz[currentQuestion].question}</h1>
        <h1 style="font-weight:bold;">Respuestas: ${numAnswers}/${numPlayers}</h1>
        `;
        socket.emit('roomInfo', { ROOM_ID: ROOM_CODE, time: currentTime, no_questions: trivia.info.no_questions, currentQuestion: currentQuestion });
        currentTime -= 1;
        await new Promise(r => setTimeout(r, 1000));
    }
}

// function to show the result of each question
const resultDraw = async() => {
    let temp = 5;
    while (temp >= 0) {
        if (temp == 3) socket.emit('getRanking', { ROOM_ID: ROOM_CODE });
        canvas.innerHTML = `
        <div style="position: absolute;left:0%;top: 0%;font-family: cursive;font-weight: bold;font-size: 150%;text-align: center;" id="timePax">Tiempo: ${temp}</div>
        <div class="container">
            <div class="question-area" id="question">${quiz[currentQuestion].question}</div>
            <div class="answer1" id="btn1"><button id="op1" style="background-color:#5cb85c;">${quiz[currentQuestion].answer}</button></div>
            <div class="answer2" id="btn2"><button id="op2" style="background-color:#d9534f;">${quiz[currentQuestion].wrong1}</button></div>
            <div class="answer3" id="btn3"><button id="op3" style="background-color:#d9534f;">${quiz[currentQuestion].wrong2}</button></div>
            <div class="answer4" id="btn4"><button id="op4" style="background-color:#d9534f;">${quiz[currentQuestion].wrong3}</button></div>
        </div>
        `;
        temp -= 1;
        await new Promise(r => setTimeout(r, 1000));
    }
}

// function to show the ranking
const rankingDraw = async() => {
    let temp = 5;

    // bubble  sort
    let len = ranking.length;
    for (let i = 0; i < len; i++) {
        for (let j = 0; j < (len - i - 1); j++) {
            if (ranking[j].score < ranking[j + 1].score) {
                let tmp = ranking[j];
                ranking[j] = ranking[j + 1];
                ranking[j + 1] = tmp;
            }
        }
    }

    let strRank = "";

    strRank = `
    <table class="table table-dark">
    <thead>
        <tr>
        <th scope="col">#</th>
        <th scope="col">Nickname</th>
        <th scope="col">score</th>
        </tr>
    </thead>
    <tbody>
    `;

    for (let i = 0; i < ranking.length; i++) {
        strRank += `<tr><td>${i+1}</td><td>${ranking[i].nickname}</td><td>${ranking[i].score}</td></tr>`;
    }

    strRank += `</tbody></table>`

    while (temp >= 0) {

        canvas.innerHTML = `
        <h2 style="color:white;text-align:center; height:30px; color:black; font-family: cursive;font-weight: bold;font-size: 36px; padding:2px;">Tiempo: ${temp}</h2>
        <br/>
        <ul class="list-group list-group-flush" id="playersList" style="margin-top:100px;" overflow-y:auto;>
        </ul>
        <div id="theEnd"></div>
        `;
        document.getElementById("playersList").innerHTML = strRank;
        temp -= 1;
        await new Promise(r => setTimeout(r, 1000));
    }
}

// time out
socket.on('finish', (data) => {
    numAnswers += 1;
    if (numAnswers == numPlayers) currentTime = -1;
});

// save the ranking
socket.on('ranking', (data) => {
    ranking = data.ranking;
});

// function to draw the host interface
const hostDraw = () => {

    canvas.innerHTML = `
    <h1 style="font-weight:bold;">Código de la sala: ${ROOM_CODE}</h1>
    <h1>Jugadores</h1>
    <ul class="list-group list-group-flush" id="playersList">
        <li class="list-group-item">...</li>
    </ul>
    <button class="start" id="startTrivia" onclick="startGame()">Iniciar trivia</button>
    <button class="start" style="background-color:#d9534f" onclick=" window.location = '/';">Cancelar</button>
    `;

    let lobbyList = document.getElementById('playersList');

    // Create the room in firebase
    socket.emit('hostRoom', { ROOM_ID: ROOM_CODE, TRIVIA_ID: triviaID });

    socket.on('updateRoom', (data) => {

        playersList = '';
        numPlayers = 0;

        for (const player in data.playersList) {
            if (data.playersList[player].nickname != undefined) {
                playersList += `<li class="list-group-item">${data.playersList[player].nickname}</li>`;
                numPlayers += 1;
            }
        }
        if (numPlayers == 0) playersList = `<li class="list-group-item">...</li>`;

        lobbyList.innerHTML = playersList;
    });
}