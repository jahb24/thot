const express = require('express');
const router = express.Router();
const controller = require('../controllers/contact');

router.get('/', (req, res) => {
    let user = req.session.user
    res.render("contact", { user });
});

router.post('/', controller.sendMessage);

module.exports = router;