const express = require('express');
const router = express.Router();
const controller = require('../controllers/index');

// GET
router.get('/', async(req, res) => {

    let info = req.flash('info');
    let user = req.session.user;

    // Get the trivias info
    let trivias = await controller.getTrivias(req.query.categorie);

    res.render("index", { user, info, trivias });
});

// Logout
router.post('/logout', controller.signOutUser);

module.exports = router;