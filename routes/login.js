const express = require('express');
const router = express.Router();
const controller = require('../controllers/login');

router.get('/', (req, res) => {

    // cookie's information
    let info = req.flash('info');
    let user = req.session.user;

    if (user) res.redirect("/");
    else res.render("login", { user, info });
});

router.post('/', controller.loginUser);

module.exports = router