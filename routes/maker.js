const express = require('express');
const router = express.Router();
const controller = require('../controllers/maker');

router.get('/', (req, res) => {
    let user = req.session.user;

    if (user) res.render("maker", { user });
    else res.redirect("/");
});

router.post('/', controller.saveTrivia);

module.exports = router;