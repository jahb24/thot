const express = require('express');
const router = express.Router();
const controller = require('../controllers/mytrivias');

// GET
router.get('/', async(req, res) => {

    let info = req.flash('info');
    let user = req.session.user;

    if (user) {

        // Get the trivias info
        let triviaList = req.session.user.trivia;
        let trivias;
        if (triviaList.length > 0) trivias = await controller.getTrivias(triviaList);

        res.render("mytrivias", { user, info, trivias });
    } else res.redirect('/');
});

// POST
router.post('/', controller.deleteTrivia);

module.exports = router;