const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    let user = req.session.user;

    if (user) res.render("profile", { user });
    else res.redirect("/");
});

module.exports = router;