const express = require('express');
const router = express.Router();
const controller = require('../controllers/ranking');

router.get('/', async(req, res) => {

    let list = await controller.getList();
    let user = req.session.user;

    res.render("ranking", { user, list });
});

module.exports = router;