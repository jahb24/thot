const express = require('express');
const router = express.Router();
const controller = require('../controllers/register');

router.get('/', (req, res) => {

    // cookie's information
    let info = req.flash('info');
    let user = req.session.user;

    if (user) res.redirect("/");
    else res.render("register", { info, user });
});

router.post('/', controller.registerUser);

module.exports = router