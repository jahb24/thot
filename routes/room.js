const express = require('express');
const router = express.Router();
const controller = require('../controllers/room');

router.get('/', (req, res) => {
    let user = req.session.user
    res.render("room", { user });
});

// Save experience
router.post('/', controller.saveExp);

module.exports = router;