const express = require('express');
const router = express.Router();
const controller = require('../controllers/trivia');

// POST
router.post('/', async(req, res) => {

    let user = req.session.user;
    let triviaID = req.body.triviaID;

    res.render("trivia", { user, triviaID });
});

module.exports = router;